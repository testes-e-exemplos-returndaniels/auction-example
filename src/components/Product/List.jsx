import ProductCard from './Card';
import './List.css';

const ProductList = ({ className, products, setBid }) => {
    return (
        <div className={`productList ${className}`}>
            {products.map((product, index)=>
            <div>
                <ProductCard
                    key={index}
                    className={`product-${index}`}
                    product={product}
                />
                <button 
                    onClick={() => 
                        setBid( 
                            {
                                id: product.id, 
                                value: product.bidAmount+5
                            }
                        )
                    }
                >
                    Aumnetar lance +5
                </button>
            </div>
            
            )}
        </div>
    ); 
}

export default ProductList;