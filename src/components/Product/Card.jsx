import './Card.css';

const ProductCard = ({ className, product }) => {
    return (
        <div className={`productCard ${className}`}>
            <img className="image" alt={product.name} src={product.image}></img>
            <div className="name"><strong>{product.name}</strong></div>
            <div className="desciption">{product.description}</div>
            <div className="timeLeft">{product.timeLeft}</div>
            <div className="bidAmount">Maior lance: {product.bidAmount}</div>
        </div>
    ); 
}

export default ProductCard