import { useEffect, useState } from "react";
import { ProductList } from '../components/Product';
import * as socketApi from '../api/socketAPI';
import './Auction.css';

var socket = socketApi.connect();
const ScreenAuction = ({ className }) => {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        socket.on("loadProducts", response => {            
            setProducts(response.data);
        });

        return () => socket.disconnect();
    }, []);
    
    const setBid = data => {
        socket.emit(
            'setBid', 
            {
                id: data.id, 
                value: data.value
            }
        );
    };

    return (
        <div className={`screenAuction ${className}`}>
            <div className='container'>
                <ProductList
                    key={1}
                    className='products'
                    products={products}
                    setBid={setBid}
                />
            </div>
        </div>
    ); 
}

export default ScreenAuction;