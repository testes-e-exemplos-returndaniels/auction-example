import io from 'socket.io-client';

const connect = (port = 4200) => {
    var socket = io.connect(`http://0.0.0.0:${port}`, {
        reconnection: true,
    });

    return socket;
};

export { connect };