import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { ScreenAuction } from '../screens';

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={ScreenAuction} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
