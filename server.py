from flask_socketio import SocketIO, emit
from flask import Flask
from flask_cors import CORS

from time import sleep

import json 

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, cors_allowed_origins="*")

CORS(app, resources={r"/*": {"origins": "*"}})

@socketio.on('connect')
def test_connect():
    print('someone connected to websocket')

    db = open('db.json',)
    data = json.load(db)

    emit(
        'loadProducts', 
        {
            'data': data['data']
        }
    )

    db.close()
 
@socketio.on('disconnect')
def disconnect():
    print('Disconnected!')

@socketio.on('setBid')
def setBid(product):    
    db = open('db.json',)
    data = json.load(db)
    
    products = data['data']
    for i in range(len(products)):
        if(products[i]['id'] == product['id']):
            products[i]['bidAmount'] = product['value']
            db = open('db.json','w')
            
            data['data'] = products
            
            json.dump(data, db)
            db.close()

    socketio.emit(
        'loadProducts', 
        {
            'data': data['data']
        }
    )

if __name__ == '__main__':
    socketio.run(app, debug=False, host='0.0.0.0', port=4200)
